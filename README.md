# enwork example integration

This repository contains an example integration of the enwork platform using [enwork CompanySync](https://www.enwork.de/company-blog/hilfe-artikel/schnellstart-enwork-companysync/) (link in German) how it could be done on a third party website. It contains both a list view of vacancies and a detailed vacany with example markup and styling. The purpose of these examples is to show you how to use enwork CompanySync and to give you a quick starting point for your own website. 

## Usage

Open the file `vacany-list.html` with your browser of choice. You will find a board of vacancies using example data from enwork. The vacancies are linked with the detailed vacancy view `vacany.html` which contains more information. 

## Note on browser support

These examples are written in plain, modern JavaScript to keep them simple and easy to understand. Parts of these modern JavaScript may not be compatible with older browsers. Please check your final integration with the browsers you'd like to support and include polyfills where necessary. 

## Support

Please [open an issue](https://gitlab.com/enwork/enwork-example-integration/-/issues/new) for support.